symbol_stack = [["void", "output", "func", [['int', 'out', 'var']]]]
#var: [type, id, semantic-type]
#fun: [type, id, semantic-type, params]
#params = [var, var, ...]
action_symbols = ["Declaration-initial"]

func = False

loop_stack = []
errors = []
#Kun esitellään looppi, se laitetaan tänne: [while, None]
#Kun alkaa statement, viiemisimmän loopin lopetusmerkki (jos none) asetetetaan.
#Jos tulee compound-statement, laitetaan }
#Jos tulee muu, laitetaan ;
#Kun statement loppuu, katsotaan aina, että lopettaako se loopin

#defined:
# 0: loop is not defined
# 1: loop is defined, but is unsure if it ends with } or ;
# 2: loop was followed by compound statement, and ends with }
# 3: loop was followed by other statement, and ends with ;

while_defined = 0
switch_defined = 0

while_depth = 0
switch_depth = 0

#Todo: no break unless while or switch, no continue unless while
#DONE: If parameter is undefined, do not warn from void. Solved by changin default type from void to None
#TODO: Write result to file

def get_referenced_symbol(semantic_type):
	last = symbol_stack[-1]
	for symbol in symbol_stack:
		if(symbol[1] == last[1] and symbol[2] == semantic_type):
			return symbol
	return None

def semantic_check(lineno, action, input):
	lineno += 1
	global func
	if action == "#def_type":
		if(func):
			symbol_stack[-1][-1].append([input])
		else:
			symbol_stack.append([input])

	elif action == "#def_id":
		if(func):
			symbol_stack[-1][-1][-1].append(input)
			symbol_stack[-1][-1][-1].append("var")
		else:
			symbol_stack[-1].append(input)
			symbol_stack[-1].append("var")

	elif(action == "#check_type"):
		if(symbol_stack[-1][2] == "var" and symbol_stack[-1][0] == "void" or symbol_stack[-1][0] == "void[]"):
			errors.append("#" + str(lineno) + ":" + "Illegal type of "+ symbol_stack[-1][0] +" for " + symbol_stack[-1][1])

	elif action == "#def_func":
		func = True
		symbol_stack[-1][2] = "func"
		symbol_stack[-1].append([])

	elif action == "#def_param":
		symbol_stack[-1][-1].append(["int", input, "var"])

	elif action == "#fun_defined":
		func = False
		for param in symbol_stack[-1][-1]:
			symbol_stack.append(param)

	elif action == "#end_func":
		while(symbol_stack[-1][2] != "func"):
			var = symbol_stack.pop()


	elif action == "#end_program":
		for fun in symbol_stack:
			if(fun[0] == "void" and fun[1] == "main" and len(fun[3]) == 0):
				return
		errors.append("#" + str(lineno) + ":" + "Main function not found!")

	elif action == "#push":
		symbol_stack.append([None, input, "exp"])
		
	
	elif action == "#set_func_call":
		symbol = get_referenced_symbol("func")
		if(symbol == None):
			errors.append("#" + str(lineno) + ":" + "Function not defined: " + symbol_stack[-1][1])
		else:
			symbol_stack[-1][0] = symbol[0]
			for param in (symbol[3])[::-1]:
				symbol_stack.append([param[0], param[1], "arg"])

	elif action == "#set_var_call":
		symbol = get_referenced_symbol("var")
		if(symbol == None):
			errors.append("#" + str(lineno) + ":" + "Variable not defined: " + symbol_stack[-1][1])
		else:
			symbol_stack[-1][0] = symbol[0]

	elif action == "#push_num":
		
		symbol_stack.append(["int", input, "exp"])

	elif action == "#expression_end" or action == "#array_access_end":
		type = None
		i = 0
		expression_params = []
		while(symbol_stack[-1][2] == "exp"):
			var = symbol_stack.pop()
			expression_params.append(var)
		if(len(expression_params) != 0 and expression_params[0][0] != "void"):
			if(action == "#array_access_end"):
				expression_params.pop()

			for var in expression_params[::-1]:
					#TODO: katso, että kaikki ovat samaa tyyppiä
				if(type != var[0] and i != 0 and type != None):
					errors.append("#" + str(lineno) + ":" + "Type mismatch. Got " + var[0] + " instead of " + type)
				type = var[0]
				i += 1


			symbol_stack.append([type, None, "exp_result"])

	elif action == "#end_func_call":
		args = []
		for symbol in symbol_stack[::-1]:
			if(symbol[2] == "exp_result"):
				args.append(symbol)
				symbol_stack.pop()
			else:
				break
		for arg in args[::-1]:
			if(symbol_stack[-1][2] == "arg"):
				if(symbol_stack[-1][0] != arg[0] and arg[0] != None): #If none, variable was not defined and error is aleady reported
					errors.append("#" + str(lineno) + ":" + "Wrong type of argument. Expected " + symbol_stack[-1][0] + ", but got " + arg[0] + " instead")
				
				symbol_stack.pop()
			else:
				errors.append("#" + str(lineno) + ":" + "Too many arguments")
		if(symbol_stack[-1][2] == "arg"):
			errors.append("#" + str(lineno) + ":" + "not enough aruments")
			while(symbol_stack[-1][2] == "arg"):
				symbol_stack.pop()

	elif(action == "#def_array"):
		if(func):
			for symbol in symbol_stack[::-1]:
				if(symbol[2] == "func"):
					symbol[3][-1][0] += "[]"
					break;
		else:
			symbol_stack[-1][0] += "[]"
	

def write_semantic_errors():
	f = open('semantic_errors.txt', 'a')
	f.truncate(0)
	if(len(errors) == 0):
		f.write("There are no semantic errors.")
	else:
		for error in errors:
			f.write(error + "\n") 

	f.close()