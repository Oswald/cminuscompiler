class Function:
    name = ""
    address = 0
    return_control_address = 0
    type = ""
    params = []
    start = -1

class Variable:
    name = ""
    address = 0
    type = ""

code = [("LT", 0, "", "")]
functions = []
# name: [address, type params]
## params: [name, type, address]

variables = []
func = False
func_definition = False

# name: address

lastId = ""
lastType = ""

lastCall = ""
expression = []

def getFunc(name):
    for f in functions:
        if f.name == name:
            return f

def getFuncAddress(name):
    f = getFunc(name)
    return f.address;

def getFuncStartLine(name):
    address = getFuncAddress(name)
    i = 0
    for line in code:
        if(line[0] == "ASSIGN" and line[2] == str(address)):
            return i
        i += 1


def getAddressOfVariable(var):
    for v in variables:
        if(var == v.name):
            return v.address

nextVarAddress = 500
def getNextVariableAddress():
    global nextVarAddress
    nextVarAddress += 4
    return nextVarAddress - 4

nextProgramAddress = 0
def getNextProgramAddress():
    global nextProgramAddress
    nextProgramAddress += 4
    return nextProgramAddress - 4

nextTempAddress = 1000
def getNextTempAddress():
    global nextTempAddress
    nextTempAddress += 4
    return nextTempAddress - 4

def calculatePriority(operation):
    if operation == "=":
        return 1;
    elif operation == "+" or operation == "-":
        return 2;
    elif operation == "*":
        return 3;
    elif operation == "<" or operation == "==":
        return 4;
    elif operation == "#func_call":
        return 5;
    else:
        return 0;
  
def handleFuncCall(name, params):

    for i in range(len(params)):
        priority = calculatePriority(params[i])
        if(priority != 0):
            l = 0

    if(name == "print"):
        generateCode("PRINT", params[0], "", "")
        return None
    else:
        line = getFuncStartLine(name)
        func = getFunc(name)
        for i in range(len(params)):
            generateCode("ASSIGN", params[i], func.params[i].address, "")
        generateCode("ASSIGN", "#" + str(len(code) + 2), func.return_control_address, "")
        generateCode("JP", "#" + str(func.start), "", "")
        #Kopioidaan paluurivi 
        #Siirretään kontrolli
        l = 1
        return func.address

def extractHighgestPriorityFromExpression(exp):
    highestPriority = -1
    highestPriorityIndex = -1
    bracket = 0

    i = 0
    for term in exp:
        if(term == "("):
            bracket += 10
        elif(term == ")"):
            bracket -= 10
        else:
            priority = bracket + calculatePriority(term)
            if(priority > highestPriority):
                highestPriority = priority
                highestPriorityIndex = i
        i += 1

    if(exp[highestPriorityIndex] == "#func_call"):
        params = []
        i = highestPriorityIndex + 1
        while(exp[i] != "#end_func_call"):
            params.append(exp[i])
            i += 1
        return_value_address = handleFuncCall(exp[highestPriorityIndex - 1], params)
        
        while(exp[highestPriorityIndex - 1] != "#end_func_call"):
            del exp[highestPriorityIndex - 1]
        exp[highestPriorityIndex - 1] = return_value_address
    else:
        address = generateCodeForExpression(exp[highestPriorityIndex - 1], exp[highestPriorityIndex], exp[highestPriorityIndex + 1])
        del exp[highestPriorityIndex - 1]
        del exp[highestPriorityIndex - 1]
        exp[highestPriorityIndex - 1] = address
    l = 0


def generateCodeForExpression(prev, operation, next):
    if operation == "=":
        generateCode("ASSIGN", next, prev, "")
        return prev
    elif(operation == "*"):
        address = getNextTempAddress()
        generateCode("MULT", prev, next, address)
        return address
    elif(operation == "+"):
        address = getNextTempAddress()
        generateCode("ADD", prev, next, address)
        return address
    elif(operation == "-"):
        address = getNextTempAddress()
        generateCode("SUB", prev, next, address)
        return address
    elif(prev == "(" and next == ")"):
        if(isinstance(operation, int) or operation[0] == "#"): #Two brackets surrounding an address
            return operation
        else:
            address = getAddressOfVariable(operation)
            return address
    else:
        l = 0

def generateCode(command, t1, t2, t3):
    code.append(createCode(command, t1, t2, t3))

def createCode(command, t1, t2, t3):
    return (command,str(t1),str(t2),str(t3))

def code_gen(action, input):
    global lastId
    global lastType
    global lastCall
    global expression
    global func
    global func_definition

    print(action, input)

    if(action == "#def_id"):
        if(func_definition):
            param = Variable()
            param.address = getNextVariableAddress()
            param.name = input
            functions[-1].params.append(param)
        else:
            lastId = input

    elif(action == "#def_type"):
        lastType = input

    elif(action == "#def_func"):
        func = True
        func_definition = True
        lastFunc = Function()
        lastFunc.address = getNextVariableAddress()
        lastFunc.return_control_address = getNextVariableAddress()
        lastFunc.name = lastId
        lastFunc.type = lastType
        lastFunc.start = len(code)
        lastFunc.params = []
        functions.append(lastFunc)
        lastId = 0
        lastType = ""
        #generateCode("ASSIGN", "#0", lastFunc.address, "")
        #generateCode("ASSIGN", "#0", lastFunc.return_control_address, "")

    elif(action == "#fun_defined"):
        func_definition = False

    elif action == "#end_func":
        func = False

    elif(action == "#check_type"):
        lastVar = Variable()
        lastVar.address = getNextVariableAddress()
        lastVar.name = lastId
        lastVar.type = lastType
        variables.append(lastVar)
        lastId = 0
        lastType = ""
        generateCode("ASSIGN", "#0", lastVar.address, "")

        
    elif(action == "#push"):
        lastCall = input
        expression.append(input)

    elif(action == "#operation"):
        expression.append(input)

    elif(action == "#bracket"):
        expression.append(input)

    elif(action == "#expression_end" and input == ";"):
        print(expression)

        for i in range(len(expression)):
            address = getAddressOfVariable(expression[i])
            if(address != None):
                expression[i] = address
            elif(func == True):
                for p in functions[-1].params:
                    if(p.name == expression[i]):
                        expression[i] = p.address

        while(len(expression) > 2):
            extractHighgestPriorityFromExpression(expression)

        if(expression[0] == "#return" and len(expression) > 1):
            func = functions[-1]
            generateCode("ASSIGN", expression[1], func.address, "")
            generateCode("JP", "@" + str(func.return_control_address), "", "")
        expression = []

    elif(action == "#push_num"):
        expression.append("#" + input)

    elif(action == "#set_func_call"):
        expression.append("#func_call")

    elif(action == "#def_param"):
        param = Variable()
        param.address = getNextVariableAddress()
        param.name = input
        functions[-1].params.append(param)

    elif(action == "#end_func_call"):
        expression.append("#end_func_call")

    elif(action == "#return"):
        expression.append("#return")
    
    elif(action == "#end_program"):
        line = getFunc("main").start
        code[0] = createCode("JP", line, "", "")
        l = 1


def writeCode():
    f = open('output.txt', 'a')
    f.truncate(0)
    i = 0
    for line in code:
        #print(str(i) + "	" + "(" + str(line[0]) + ", " + str(line[1]) + ", " + str(line[2]) + ", " + str(line[3]) + ")")
        f.write(str(i) + "	" + "(" + str(line[0]) + ", " + str(line[1]) + ", " + str(line[2]) + ", " + str(line[3]) + ")" + "\n") 
        i += 1
    
    f.close()