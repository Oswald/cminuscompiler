from scanner import get_next_token
from scanner import initialize
from semantic_analyser import semantic_check, write_semantic_errors
from code_gen import code_gen, writeCode

follows = {
    "Program": ["$"],
    "Declaration-list": ["$", "int", "void", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "}", "else", "default", "case"],
    "Declaration": ["$", "int", "void", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "}", "else", "default", "case"],
    "Declaration-initial": ["(", ";", "[", ",", ")"],
    "Declaration-prime": ["$", "int", "void", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "}", "else", "default", "case"],
    "Var-declaration-prime": ["$", "int", "void", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "}", "else", "default", "case"],
    "Fun-declaration-prime": ["$", "int", "void", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "}", "else", "default", "case"],
    "Type-specifier": ["ID"],
    "Params": [")"],
    "Param-list-void-abtar": [")"],
    "Param-list": [")"],
    "Param": [",", ")"],
    "Param-prime": [",", ")"],
    "Compound-stmt": ["$", "int", "void", "}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "else", "default", "case"],
    "Statement-list": ["}", "default", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "case", "else"],
    "Statement": ["}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "else", "default", "case"],
    "Expression-stmt": ["}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "else", "default", "case"],
    "Selection-stmt": ["}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "else", "default", "case"],
    "Iteration-stmt": ["}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "else", "default", "case"],
    "Return-stmt": ["}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "else", "default", "case"],
    "Return-stmt-prime": ["}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "else", "default", "case"],
    "Switch-stmt": ["}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "else", "default", "case"],
    "Case-stmts": ["default", "}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "else", "case"],
    "Case-stmt": ["default", "}", "{", "continue", "break", ";", "if", "while", "return", "switch", "ID", "(", "NUM", "case", "else"],
    "Default-stmt": ["}"],
    "Expression": [";", ")", "]", ","],
    "B": [";", ")", "]", ","],
    "H": [";", ")", "]", ","],
    "Simple-expression-zegond": [";", ")", "]", ","],
    "Simple-expression-prime": [";", ")", "]", ","],
    "C": [";", ")", "]", ","],
    "Relop": ["(", "ID", "NUM"],
    "Additive-expression": [";", ")", "]", ","],
    "Additive-expression-prime": [";", ")", "<", "==", "]", ","],
    "Additive-expression-zegond": [";", ")", "<", "==", "]", ","],
    "D": [";", ")", "<", "==", "]", ","],
    "Addop": ["(", "ID", "NUM"],
    "Term": [";", ")", "+", "-", "<", "==", "]", ","],
    "Term-prime": [";", ")", "<", "==", "+", "-", "]", ","],
    "Term-zegond": [";", ")", "<", "==", "+", "-", "]", ","],
    "G": [";", ")", "+", "-", "<", "==", "]", ","],
    "Factor": ["*", ";", ")", "+", "-", "<", "==", "]", ","],
    "Var-call-prime": ["*", ";", ")", "+", "-", "<", "==", "]", ","],
    "Var-prime": ["*", ";", ")", "+", "-", "<", "==", "]", ","],
    "Factor-prime": ["*", ";", ")", "<", "==", "+", "-", "]", ","],
    "Factor-zegond": ["*", ";", ")", "<", "==", "+", "-", "]", ","],
    "Args": [")"],
    "Arg-list": [")"],
    "Arg-list-prime": [")"]
}

parsing_table = [[0,"ID",";","[","NUM","]","(",")","int","void",",","{","}","continue","break","if","else","while","return","switch","case",":","default","=","<","==","+","-","*","$"],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,90,90,90,90,90,90,90, 1, 1,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90, 1], #2
            [0, 3,90,90, 3,90, 3,90, 2, 2,90, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,90, 3,90,90,90,90,90,90, 3],
            [0,90,90,90,90,90,90,90, 4, 4,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90, 5, 5,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90, 7, 7,90,90, 6,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90, 8, 9,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,10,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90,11,12,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90,13,14,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90], #10
            [0,15,90,90,90,90,90,16,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,18,90,90,17,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90,19,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,20,90,90,90,21,90,90,21,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90,90,90,90,22,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,23,23,90,23,90,23,90,90,90,90,23,24,23,23,23,24,23,23,23,24,90,24,90,90,90,90,90,90,90],
            [0,25,25,90,25,90,25,90,90,90,90,26,90,25,25,27,90,28,29,30,90,90,90,90,90,90,90,90,90,90],
            [0,31,34,90,31,90,31,90,90,90,90,90,90,32,33,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90,90,90,90,90,90,90,90,35,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,36,90,90,90,90,90,90,90,90,90,90,90,90], #20
            [0,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,37,90,90,90,90,90,90,90,90,90,90,90],
            [0,39,38,90,39,90,39,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,40,90,90,90,90,90,90,90,90,90,90],
            [0,42,42,90,42,90,42,90,90,90,90,42,42,42,42,42,42,42,42,42,41,90,42,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,43,90,90,90,90,90,90,90,90,90],
            [0,90,90,90,90,90,90,90,90,90,90,90,45,90,90,90,90,90,90,90,90,90,44,90,90,90,90,90,90,90],
            [0,47,90,90,46,90,46,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,50,49,90,50,50,50,90,90,50,90,90,90,90,90,90,90,90,90,90,90,90,48,50,50,50,50,50,90],
            [0,90,52,90,90,52,90,52,90,90,52,90,90,90,90,90,90,90,90,90,90,90,90,51,52,52,52,52,52,90],
            [0,90,90,90,53,90,53,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90], #30
            [0,90,54,90,90,54,54,54,90,90,54,90,90,90,90,90,90,90,90,90,90,90,90,90,54,54,54,54,54,90],
            [0,90,56,90,90,56,90,56,90,90,56,90,90,90,90,90,90,90,90,90,90,90,90,90,55,55,90,90,90,90],
            [0,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,57,58,90,90,90,90],
            [0,59,90,90,59,90,59,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,60,90,90,60,60,60,90,90,60,90,90,90,90,90,90,90,90,90,90,90,90,90,60,60,60,60,60,90],
            [0,90,90,90,61,90,61,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,63,90,90,63,90,63,90,90,63,90,90,90,90,90,90,90,90,90,90,90,90,90,63,63,62,62,90,90],
            [0,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,64,65,90,90],
            [0,66,90,90,66,90,66,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,67,90,90,67,67,67,90,90,67,90,90,90,90,90,90,90,90,90,90,90,90,90,67,67,67,67,67,90], #40
            [0,90,90,90,68,90,68,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,70,90,90,90,90,70,90,90,70,90,90,90,90,90,90,90,90,90,90,90,90,90,70,70,70,70,69,90],
            [0,72,90,90,73,90,71,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,90,75,75,90,90,74,75,90,90,75,90,90,90,90,90,90,90,90,90,90,90,90,90,75,75,75,75,75,90],
            [0,90,77,76,90,90,77,77,90,90,77,90,90,90,90,90,90,90,90,90,90,90,90,90,77,77,77,77,77,90],
            [0,90,79,90,90,79,78,79,90,90,79,90,90,90,90,90,90,90,90,90,90,90,90,90,79,79,79,79,79,90],
            [0,90,90,90,81,90,80,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,82,90,90,82,90,82,83,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90],
            [0,84,90,90,84,90,84,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90], 
            [0,90,90,90,90,90,90,86,90,90,85,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90]]

expressions = [
    [],
    ["Declaration-list", "#end_program"],                                   #1
    ["Declaration", "Declaration-list"],
    ["$"],
    ["Declaration-initial", "Declaration-prime"],
    ["#def_type", "Type-specifier", "#def_id", "ID"],
    ["#def_func", "Fun-declaration-prime"],
    ["Var-declaration-prime"],
    ["#check_type", ";"],
    ["#def_array", "[", "NUM", "]", "#check_type", ";"],
    ["(", "Params", "#fun_defined", ")", "Compound-stmt", "#end_func"],     #10
    ["int"],
    ["void"],
    ["int", "#def_param", "ID", "Param-prime", "Param-list"],
    ["void", "Param-list-void-abtar"],
    ["ID", "Param-prime", "Param-list"],
    ["$"],
    [",", "Param", "Param-list"],
    ["$"],
    ["Declaration-initial", "Param-prime"],
    ["#def_array", "[", "]"],                                               #20
    ["$"],
    ["{", "Declaration-list", "Statement-list", "}"],
    ["Statement", "Statement-list"],
    ["$"],
    ["Expression-stmt"],
    ["Compound-stmt"],
    ["Selection-stmt"],
    ["Iteration-stmt"],
    ["Return-stmt"],
    ["Switch-stmt"],                                                        #30
    ["Expression", "#expression_end", ";"],
    ["continue", ";"],
    ["break", ";"],
    [";"],
    ["if", "(", "Expression", "#expression_end", ")", "Statement", "else", "Statement"],
    ["while", "(", "Expression", "#expression_end", ")", "Statement"],
    ["#return", "return", "Return-stmt-prime"],
    [";"],
    ["Expression", "#expression_end", ";"],
    ["switch", "(", "Expression", "#expression_end", ")", "{", "Case-stmts", "Default-stmt", "}"], #40
    ["Case-stmt", "Case-stmts"],
    ["$"],
    ["case", "NUM", ":", "Statement-list"],
    ["default", ":", "Statement-list"],
    ["$"],
    ["Simple-expression-zegond"],
    ["#push", "ID", "B"],
    ["#set_var_call", "#operation", "=", "Expression"],
    ["[", "Expression", "#array_access_end", "]", "H"],
    ["Simple-expression-prime"],                                            #50
    ["#operation", "=", "Expression"],
    ["G", "D", "C"],
    ["Additive-expression-zegond", "C"],
    ["Additive-expression-prime", "C"],
    ["Relop", "Additive-expression"],
    ["$"],
    ["#operation","<"],
    ["#operation","=="],
    ["Term", "D"],
    ["Term-prime", "D"],                                                    #60
    ["Term-zegond", "D"],
    ["Addop", "Term", "D"],
    ["$"],
    ["#operation","+"],
    ["#operation","-"],
    ["Factor", "G"],
    ["Factor-prime", "G"],
    ["Factor-zegond", "G"],
    ["#operation","*", "Factor", "G"],
    ["$"],                                                                  #70
    ["#bracket", "(", "Expression", "#bracket", "#expression_end",  ")"],
    ["#push","ID", "Var-call-prime"],
    ["#push_num", "NUM"],
    ["#set_func_call", "(", "Args", "#end_func_call", ")"],
    ["#set_var_call", "Var-prime"],
    ["[", "Expression", "#array_access_end", "]"],
    ["$"],
    ["#set_func_call", "(", "Args", "#end_func_call", ")"],
    ["#set_var_call", "$"],
    ["#bracket","(", "Expression","#bracket", "#expression_end", ")"],                                               #80
    ["#push_num", "NUM"],
    ["Arg-list"],
    ["$"],
    ["Expression", "#expression_end", "Arg-list-prime"],
    [",", "Expression", "#expression_end", "Arg-list-prime"],
    ["$"]
]

non_terminals = [
    "Program",
    "Declaration-list",
    "Declaration",
    "Declaration-initial",
    "Declaration-prime",
    "Var-declaration-prime",
    "Fun-declaration-prime",
    "Type-specifier",
    "Params",
    "Param-list-void-abtar",
    "Param-list",
    "Param",
    "Param-prime",
    "Compound-stmt",
    "Statement-list",
    "Statement",
    "Expression-stmt",
    "Selection-stmt",
    "Iteration-stmt",
    "Return-stmt",
    "Return-stmt-prime",
    "Switch-stmt",
    "Case-stmts",
    "Case-stmt",
    "Default-stmt",
    "Expression",
    "B",
    "H",
    "Simple-expression-zegond",
    "Simple-expression-prime",
    "C",
    "Relop",
    "Additive-expression",
    "Additive-expression-prime",
    "Additive-expression-zegond",
    "D",
    "Addop",
    "Term",
    "Term-prime",
    "Term-zegond",
    "G",
    "Factor",
    "Var-call-prime",
    "Var-prime",
    "Factor-prime",
    "Factor-zegond",
    "Args",
    "Arg-list",
    "Arg-list-prime"
]

stack = [[0, "$"], [0, "Program"]]
tree = []
#Tallennetaan, kuinka monta childiä syvyydeltä lähtee. Kun molemmat childit on popattu, ei piirretä enää oksaa.

initialize()

def writeTree(content):
    f = open('parse_tree.txt', 'a')
    f.truncate(0)
    f.write(content)
    f.close()


def writeSyntaxErrors(errors):
    f = open('syntax_errors.txt', 'a')
    f.truncate(0)
    if(len(errors) == 0):
        f.write("There is no syntax errors.")
    else:
        for error in errors:
            f.write("#" + str(error[0] + 1) + ' :	' + error[1] + '\n') 
        f.close()

def getTokenContent():
    if(token[0] == "KEYWORD" or token[0] == "SYMBOL" or token[0] == "EPSILON"):
        return token[1]
    else:
        return token[0]

def createTokenString(t):
    return "(" + token[0] + ", " + token[1] + ")"


token_data = get_next_token()
token = token_data[1]
content = getTokenContent()

file = ""
syntaxErrors = []


while(True):
    #If there is non-terminal on the stack, do not fetch new token
    if(content == "$" and len(stack) == 0):
        break;

    stack_data = stack.pop()
    top = stack_data[1]
    depth = stack_data[0]

    while(top[0] == "#"):
        stack_data = stack.pop()
        depth = stack_data[0]
        semantic_check(token_data[0], top, token[1])
        #code_gen(top, token[1])
        top = stack_data[1]

    string = ""

    for i in range(0, depth):
        if(tree[i] > 0):
            if(i + 1 == depth):
                if(tree[i] == 1):
                    string += "|-- "
                else:
                    string += "|-- "
                tree[i] -= 1
            else:
                string += "|   "
        else:
            string += "    "
    
        
    if(top in non_terminals):
        string += top

        column = parsing_table[0].index(content)
    
        row = non_terminals.index(top) + 2
        expression_index = parsing_table[row][column]


        if(expression_index == 90):
            if(content in follows[top]):
                syntaxErrors.append([token_data[0], "Missing token from " + top])
                continue

            else:
                if(content == "$"):
                    syntaxErrors.append([token_data[0], "Unexpected EndOfFile"])
                    break;
                else:
                    syntaxErrors.append([token_data[0], "Illegal " + content])
                    token_data = get_next_token()
                    token = token_data[1]
                    content = getTokenContent()
                    stack.append([depth, top])
                    continue

        expression = expressions[expression_index]

        if(len(tree) <= depth):
            tree.append(0)

        tree[depth] += len(expression)

        for item in expression[::-1]:
            stack.append([depth + 1, item])
            
            
   
    elif(top == "$"):
        if(not len(stack) == 0):
            string += "epsilon"
    else:
        string += createTokenString(token)

        if(not content == top):
            syntaxErrors.append([token_data[0], "Illegal " + content])
            stack.append([depth, top])
        
        token_data = get_next_token()
        token = token_data[1]
        content = getTokenContent()

    file += string
    file += "\n"

writeTree(file)
print(file)
writeSyntaxErrors(syntaxErrors)
write_semantic_errors()
#writeCode()