#Oskari Lehti, 477264

characters = []
index = 0
keywords = ["if", "else", "void", "int", "while", "break", "continue", "switch", "default", "case", "return"]
symbols = [';', ':', ',', '[', ']', '(', ')', '{', '}', '+', '-', '*', '=', '<', '==']
symbolTable = []
lines = [[]]
errors = []

def getCharacterFromCurrentIndex():
	global index
	if(index == len(characters)):
		raise Exception("End of file")
	char = characters[index]
	index += 1
	return char

def peek():
	global index
	if(index == len(characters)):
		return ''
	return characters[index]

def reverseIndex():
	global index
	index -= 1

def removeCharsUntilEndOfLine():
	char = getCharacterFromCurrentIndex()
	while(char != '\n'):
		char = getCharacterFromCurrentIndex()
	lines.append([])

def removeCharsUntilEndOfComment():
	previous = getCharacterFromCurrentIndex()
	char = getCharacterFromCurrentIndex()
	while(not (previous == "*" and char == '/')):
		previous = char
		char = getCharacterFromCurrentIndex()


def getNextChar(): #Returns next character, removing comments
	char = getCharacterFromCurrentIndex()

	if(char == '\n'):
		lines.append([])
	elif(char == '/'):
		nextChar = getCharacterFromCurrentIndex()
		if(nextChar == '/'):
			# //, remove chars until end of line
			removeCharsUntilEndOfLine()
		elif(nextChar == '*'):
			try:
				removeCharsUntilEndOfComment()
			except Exception as e:
				errors.append((len(lines), "Unclosed comment. '*/' expexted.", ""))
				raise e
		else:
			reverseIndex()
			return char;
		return getNextChar()
	return char;
	

def isIdentifier(tokenString):
	if(tokenString[0].isalpha()):
		for c in tokenString:
			if(not c.isalnum()):
				return False
		return True
	else:
		return False

def parseIlFormattedNumber(tokenString):
	token = ""
	i = 1
	for char in tokenString:
		if(char.isnumeric()):
			i += 1
		else:
			errors.append((len(lines), "Illegal number", tokenString[:i]))
			return tokenString[i:]

def parseError(tokenString):
	reversed = tokenString[::-1]
	token = ""
	for char in reversed:
		if(char.isalpha() or char.isnumeric()):
			token += char
		else:
			errors.append((len(lines), "Invalid input", tokenString[:len(tokenString) - len(token)]))
			return token[::-1]
	return parseIlFormattedNumber(tokenString)


def createToken(tokenString):
	if(not tokenString):
		return
	elif(tokenString in keywords):
		return ("KEYWORD", tokenString)
	elif(tokenString in symbols):
		return ("SYMBOL", tokenString)
	elif(tokenString.isnumeric()):
		return ("NUM", tokenString)
	elif(isIdentifier(tokenString)):
		return ("ID", tokenString)
	else:
		return createToken(parseError(tokenString))

def isSymbol(char):
	return char in symbols

def addToSymbolTable(token):
	if((token[0] == "ID") and token[1] not in symbolTable):
		symbolTable.append(token[1])


def get_next_token():
	token = get_next()
	return token

def get_next():

	token = ""
	result = []
	lineno = None

	try:

		while(True):
			char = getNextChar()

			if(char.isspace()):
				if(token):
					result = createToken(token)
					break
			elif(not lineno): #Save line number
				lineno = len(lines) - 1

			if(isSymbol(char)):
				if(token):
					reverseIndex()
					result = createToken(token)
					break
				else:
					#if == (or theoretically other multi-char -symbols)
					if(char + peek() in symbols):
						result = createToken(char + getNextChar())
						break
					elif(char == '*' and peek() == '/'):
						getCharacterFromCurrentIndex()
						errors.append((len(lines), "Unmatched */", "*/"))
						break
					else:
						result = createToken(char)
						break
			elif(not char.isspace()):
				token = token + char
		if(result):
			addToSymbolTable(result)
			return (lineno, result)
		else:
			#If no valid token was created, call recursively
			return get_next_token()
	except Exception as e:
		return (lineno, ("EPSILON", '$'))

def writeTokens():
	f = open('tokens.txt', 'a')
	f.truncate(0)

	i = 1
	for row in lines:
		if(len(row) > 0):
			line =  str(i) + ".	"
			for token in row:
				line += '(' + token[0] + ', ' + token[1] + ') '

			f.write(line.rstrip() + '\n')
		i += 1
	
	f.close()

def writeErrors():
	f = open('lexical_errors.txt', 'a')
	f.truncate(0)
	if(len(errors) == 0):
		f.write("There is no lexical errors.")
	else:
		for error in errors:
			f.write(str(error[0]) + ',	(' + error[2] + ', ' + error[1] + ')\n') 

	f.close()

def writeSymbolTable():
	f = open('symbol_table.txt', 'a')
	f.truncate(0)

	i = 1
	for symbol in symbolTable:
		f.write(str(i) + '.	' + symbol + '\n')
		i += 1
	f.close()

def initialize():
#Read the file to array, as there were some OS-dependent issues with seeking at the end of the file
	file = open("input.txt")
	char = file.read(1)
	while char:
		characters.append(char)
		char = file.read(1)
	characters.append('\0')

	for keyword in keywords:
		symbolTable.append(keyword)

try:
	while(True):
		token = get_next_token()
		#(lineno, token)
		lines[token[0]].append(token[1])
		if(token[1][0] == "EPSILON"):
			break;
except Exception as e:
	writeTokens()
	writeErrors()
	writeSymbolTable()








